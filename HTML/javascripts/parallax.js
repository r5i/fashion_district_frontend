$(document).ready(function() {
    $(window).bind('scroll', function(e) {
        parallax();
    });
});

function parallax() {
    var scrollPosition = $(window).scrollTop();
	var pWindow = $(window).width();
    $('.hero-cover').css('top',(0 - (scrollPosition * -.45))+'px' );
	$('.sub-hero').css('top',(0 - (scrollPosition * -.45))+'px' );
	if(pWindow > 1400){
		$('.newsletter-bg').css('top',(0 - (scrollPosition * .15))+'px' );
	}else if((pWindow >991) && (pWindow<1400)){
		$('.newsletter-bg').css('top',(0 - (scrollPosition * .18))+'px' );
	}else if((pWindow >767) && (pWindow<992)){
		$('.newsletter-bg').css('top',(0 - (scrollPosition * .15))+'px' );
	}else if((pWindow >300) && (pWindow<768)){
		$('.newsletter-bg').css('top',(0 - (scrollPosition * .064))+'px' );
	}
}       